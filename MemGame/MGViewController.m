//
//  MGViewController.m
//  MemGame
//
//  Created by Johnykutty on 08/09/14.
//  Copyright (c) 2014 Johnykutty. All rights reserved.
//

#import "MGViewController.h"
#import "MGFlickrFeedLoader.h"
#import "MGTileView.h"
#import "UIImageView+WebCache.h"
#import "MGFeedItem.h"

@interface MGViewController ()<MGTileViewDelegate>{
    NSInteger currentShowingTile;
    NSInteger numberOfImagesLoaded;
    NSInteger secondsRemaining;
    NSMutableArray *correctedTiles;
}
@property (weak, nonatomic) IBOutlet UIImageView *previewImage;
@property (weak, nonatomic) IBOutlet UIView *tileContainer;
@property (weak, nonatomic) IBOutlet UILabel *instructionLabel;

@property (strong, nonatomic) NSTimer *timer;
@end

@implementation MGViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self startGame];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIAlertView delegates
/**
 *  Called when a button is clicked.
 *
 *  @param alertView   alertView sending this message
 *  @param buttonIndex index of button in which user is taped
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == alertView.firstOtherButtonIndex) {
        [self startGame];
    }
}

#pragma mark - Tileview delegates

/**
 *  Game starts from here. 
 *          This method initiate loading public feed from flickr
 */
- (void)startGame
{
    [self.previewImage setImage:nil];
    [self.tileContainer.subviews makeObjectsPerformSelector:@selector(setImage:) withObject:nil];
    
    self.instructionLabel.text = @"Loading flicker feed...";
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [MGFlickrFeedLoader load:9 withCompletionHandler:^(id result, NSError *error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if (error) {
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles: nil];
            [alertview show];
        }
        else if ([result count]<9){
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:nil message:@"Couln't load all images\nPlease try again later" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles: nil];
            [alertview show];
        }
        else{
            [self loadImagesForFeed:result];
        }
    }];
}

/**
 *  Set feedItem property of each tileview so that it will initiate loading images
 *
 *  @param feeds array of MGFeedItem loaded from flickr
 */
- (void)loadImagesForFeed:(NSArray *)feeds{
    self.instructionLabel.text = @"Loading images...";
    
    numberOfImagesLoaded = 0;
    [self.tileContainer.subviews enumerateObjectsUsingBlock:^(MGTileView *tileView, NSUInteger idx, BOOL *stop) {
        [tileView setFeedItem:feeds[idx]];
        tileView.delegate = self;
    }];
}

/**
 *  Invoke when a tile complets downloading its image from url
 *
 *  @param tile - tile view object sending this message
 */
- (void)tileViewDidLoadImage:(MGTileView *)tile{
    numberOfImagesLoaded ++;
    if (numberOfImagesLoaded >= 9) {
        secondsRemaining = 15;
        [self.tileContainer.subviews makeObjectsPerformSelector:@selector(flipToOriginalImage)];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
    }
}

/**
 *  Get called when user tap on a tile
 *
 *  @param tile - tile view object sending this message
 */
- (void)tileViewDidTapped:(MGTileView *)tile{
    [tile flipToOriginalImage];
    if (tile.tag == currentShowingTile) {
        [correctedTiles addObject:@(currentShowingTile)];
        if (correctedTiles.count >= 9) {
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Congradulations, You won !!!" message:@"Play again?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            [alertview show];
        }
        else{
            [self selectRandomTileAndDisplay];
        }
    }
    else{
        [tile performSelector:@selector(fliptoPlaceHolder) withObject:nil afterDelay:1.f];
    }
}

#pragma mark - Utility methods
/**
 *  Timer Tick
 *
 *  @param timer timer object sending this message
 */
- (void)timerTick:(NSTimer *)timer{
    secondsRemaining --;
    self.instructionLabel.text = [NSString stringWithFormat:@"Memorise image positions in %d seconds",secondsRemaining];
    if (secondsRemaining <= 0) {
        self.instructionLabel.text = @"Select position of image shown in top";
        [self.timer invalidate];
        timer = nil;
        correctedTiles = [NSMutableArray new];
        [self.tileContainer.subviews makeObjectsPerformSelector:@selector(fliptoPlaceHolder)];
        [self selectRandomTileAndDisplay];
    }
}

/**
 *  Display image from random tile from 9 tiles to the preview 
 */
- (void)selectRandomTileAndDisplay{
    NSInteger randomTile = -1;
  
    do {
        randomTile = arc4random_uniform(9)+ 1;
    } while ([correctedTiles containsObject:@(randomTile)]);
    currentShowingTile = randomTile;
    MGTileView *tileView = (MGTileView *)[self.tileContainer viewWithTag:currentShowingTile];
    [self.previewImage sd_setImageWithURL:tileView.feedItem.photoURL];
}

@end
