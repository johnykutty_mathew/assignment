//
//  MGAppDelegate.h
//  MemGame
//
//  Created by Johnykutty on 08/09/14.
//  Copyright (c) 2014 Johnykutty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
