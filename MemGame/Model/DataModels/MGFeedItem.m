//
//  MGFeedItem.m
//
//  Created by Johnykutty  on 09/09/14
//  Copyright (c) 2014 Johnykutty. All rights reserved.
//

#import "MGFeedItem.h"


NSString *const kMGImageItemAuthor = @"author";
NSString *const kMGImageItemAuthorId = @"author_id";
NSString *const kMGImageItemPublished = @"published";
NSString *const kMGImageItemTitle = @"title";
NSString *const kMGImageItemphotoURL = @"media.m";
NSString *const kMGImageItemLink = @"link";
NSString *const kMGImageItemDateTaken = @"date_taken";
NSString *const kMGImageItemDescription = @"description";
NSString *const kMGImageItemTags = @"tags";


@interface MGFeedItem ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation MGFeedItem

@synthesize author = _author;
@synthesize authorId = _authorId;
@synthesize published = _published;
@synthesize title = _title;
@synthesize photoURL = _photoURL;
@synthesize link = _link;
@synthesize dateTaken = _dateTaken;
@synthesize itemDescription = _itemDescription;
@synthesize tags = _tags;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.author = [self objectOrNilForKey:kMGImageItemAuthor fromDictionary:dict];
            self.authorId = [self objectOrNilForKey:kMGImageItemAuthorId fromDictionary:dict];
            self.published = [self objectOrNilForKey:kMGImageItemPublished fromDictionary:dict];
            self.title = [self objectOrNilForKey:kMGImageItemTitle fromDictionary:dict];

            NSString *photoURL = [self objectOrNilForKey:kMGImageItemphotoURL fromDictionary:dict];
            self.photoURL = [NSURL URLWithString:photoURL];
        
            self.link = [self objectOrNilForKey:kMGImageItemLink fromDictionary:dict];
            self.dateTaken = [self objectOrNilForKey:kMGImageItemDateTaken fromDictionary:dict];
            self.itemDescription = [self objectOrNilForKey:kMGImageItemDescription fromDictionary:dict];
            self.tags = [self objectOrNilForKey:kMGImageItemTags fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.author forKey:kMGImageItemAuthor];
    [mutableDict setValue:self.authorId forKey:kMGImageItemAuthorId];
    [mutableDict setValue:self.published forKey:kMGImageItemPublished];
    [mutableDict setValue:self.title forKey:kMGImageItemTitle];
    [mutableDict setValue:self.photoURL forKey:kMGImageItemphotoURL];
    [mutableDict setValue:self.link forKey:kMGImageItemLink];
    [mutableDict setValue:self.dateTaken forKey:kMGImageItemDateTaken];
    [mutableDict setValue:self.itemDescription forKey:kMGImageItemDescription];
    [mutableDict setValue:self.tags forKey:kMGImageItemTags];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict valueForKeyPath:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.author = [aDecoder decodeObjectForKey:kMGImageItemAuthor];
    self.authorId = [aDecoder decodeObjectForKey:kMGImageItemAuthorId];
    self.published = [aDecoder decodeObjectForKey:kMGImageItemPublished];
    self.title = [aDecoder decodeObjectForKey:kMGImageItemTitle];
    self.photoURL = [aDecoder decodeObjectForKey:kMGImageItemphotoURL];
    self.link = [aDecoder decodeObjectForKey:kMGImageItemLink];
    self.dateTaken = [aDecoder decodeObjectForKey:kMGImageItemDateTaken];
    self.itemDescription = [aDecoder decodeObjectForKey:kMGImageItemDescription];
    self.tags = [aDecoder decodeObjectForKey:kMGImageItemTags];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_author forKey:kMGImageItemAuthor];
    [aCoder encodeObject:_authorId forKey:kMGImageItemAuthorId];
    [aCoder encodeObject:_published forKey:kMGImageItemPublished];
    [aCoder encodeObject:_title forKey:kMGImageItemTitle];
    [aCoder encodeObject:_photoURL forKey:kMGImageItemphotoURL];
    [aCoder encodeObject:_link forKey:kMGImageItemLink];
    [aCoder encodeObject:_dateTaken forKey:kMGImageItemDateTaken];
    [aCoder encodeObject:_itemDescription forKey:kMGImageItemDescription];
    [aCoder encodeObject:_tags forKey:kMGImageItemTags];
}

- (id)copyWithZone:(NSZone *)zone
{
    MGFeedItem *copy = [[MGFeedItem alloc] init];
    
    if (copy) {

        copy.author = [self.author copyWithZone:zone];
        copy.authorId = [self.authorId copyWithZone:zone];
        copy.published = [self.published copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.photoURL = [self.photoURL copyWithZone:zone];
        copy.link = [self.link copyWithZone:zone];
        copy.dateTaken = [self.dateTaken copyWithZone:zone];
        copy.itemDescription = [self.itemDescription copyWithZone:zone];
        copy.tags = [self.tags copyWithZone:zone];
    }
    
    return copy;
}


@end
