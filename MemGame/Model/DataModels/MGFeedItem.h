//
//  MGFeedItem.h
//
//  Created by Johnykutty  on 09/09/14
//  Copyright (c) 2014 Johnykutty. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MGFeedItem : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *author;
@property (nonatomic, strong) NSString *authorId;
@property (nonatomic, strong) NSString *published;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSURL *photoURL;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *dateTaken;
@property (nonatomic, strong) NSString *itemDescription;
@property (nonatomic, strong) NSString *tags;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
