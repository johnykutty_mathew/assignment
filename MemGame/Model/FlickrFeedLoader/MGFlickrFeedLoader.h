//
//  MGFlickrFeedLoader.h
//  MemGame
//
//  Created by Johnykutty on 09/09/14.
//  Copyright (c) 2014 Johnykutty. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^MGFlickrFeedLoaderCompletionBlock)(id result,NSError *error) ;

@interface MGFlickrFeedLoader : NSObject
+ (void)load:(NSInteger)count withCompletionHandler:(MGFlickrFeedLoaderCompletionBlock)block;
@end
