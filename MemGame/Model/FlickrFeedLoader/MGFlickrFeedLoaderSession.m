//
//  MGFlickrFeedLoaderSession.m
//  MemGame
//
//  Created by Johnykutty on 10/09/14.
//  Copyright (c) 2014 Johnykutty. All rights reserved.
//

#import "MGFlickrFeedLoaderSession.h"

@implementation MGFlickrFeedLoaderSession

/**
 *  Load feed with NSURLSession
 *
 *  @param feedURL url to be loaded
 *  @param block completion block to be excecuted after loading feed
 */
+ (void)loadFeedWithURL:(NSURL *)feedURL completionHandler:(MGFlickrFeedLoaderCompletionBlock)block{
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithURL:feedURL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        block(data,error);
    }];
    [task resume];
}

@end
