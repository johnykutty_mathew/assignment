//
//  MGFlickrFeedLoader.m
//  MemGame
//
//  Created by Johnykutty on 09/09/14.
//  Copyright (c) 2014 Johnykutty. All rights reserved.
//

#import "MGFlickrFeedLoader.h"
#import "MGFlickrFeedLoaderConnection.h"
#import "MGFlickrFeedLoaderSession.h"
#import "MGFeedItem.h"

static NSString *kFlickerPublicFeedURL = @"https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1";

/**
 *  Interface extension for MGFlickrFeedLoader
 */
@interface MGFlickrFeedLoader ()

+ (void)loadFeedWithURL:(NSURL *)feedURL completionHandler:(MGFlickrFeedLoaderCompletionBlock)block;

@end

@implementation MGFlickrFeedLoader
/**
 *  Load public feed from flickr, Here implementing class cluster.
 *       If NSURLSession is available,
 *       MGFlickrFeedLoaderSession class will load feed with NSURLSession.
 *       Otherwise MGFlickrFeedLoaderConnection class will load with NSURLConnection
 *
 *  @param count number of feeds needed by caller
 *  @param block completion block to be excecuted after loading feed
 */
+ (void)load:(NSInteger)count withCompletionHandler:(MGFlickrFeedLoaderCompletionBlock)block{
    NSURL *feedURL = [NSURL URLWithString:kFlickerPublicFeedURL];
    if (NSClassFromString(@"NSURLSession")) {
        [MGFlickrFeedLoaderSession loadFeedWithURL:feedURL completionHandler:^(id result, NSError *error) {
            [self handleResponse:result error:error forCount:count andCompletionHandler:block];
        }];
    }
    else{
        [MGFlickrFeedLoaderConnection loadFeedWithURL:feedURL completionHandler:^(id result, NSError *error) {
            [self handleResponse:result error:error forCount:count andCompletionHandler:block];
        }];
    }
}
/**
 *  Subclasses should implement this. Empty implementation is to avoid warning
 */
+ (void)loadFeedWithURL:(NSURL *)feedURL completionHandler:(MGFlickrFeedLoaderCompletionBlock)block{
    
}

/**
 *  Handle response of loading flickr feed.
 *       Convert row data to json,
 *       Parsing json to model classes done in this method,
 *       and selecting random 'count' feeds from feed
 *
 *  @param result raw data loaded from flickr
 *  @param error  if any eror returned from network operations
 *  @param count  number of feeds needed
 *  @param block  completion block to be excecuted after loading feed
 */
+ (void)handleResponse:(id)result error:(NSError *)error forCount:(NSInteger)count andCompletionHandler:(MGFlickrFeedLoaderCompletionBlock)block{
    /*
     The Flickr API has a special feature that escapes single quotes in the JSON response. The problem is that this renders the JSON response invalid according to the current standards and, as a result, the NSJSONSerialization API cannot process it.
     To fix this, only need to remove the escaped single quotes in the JSON response.
     
     Reference *http://code.tutsplus.com/tutorials/beyond-the-basics-of-jsonmodel--cms-20731
     */
    NSString *responseString = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"\\'" withString:@"'"];

    if (error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            block(nil,error);
        });
        return ;
    }
    NSError *parseError = nil;
    id response = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:&error];
    if (parseError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            block(nil,parseError);
        });
        return ;
    }
    id items = [response isKindOfClass:[NSDictionary class]] ? response[@"items"] : nil;
    items = [items isKindOfClass:[NSArray class]] ? [items mutableCopy] : nil;

    NSMutableArray *feedItems = [NSMutableArray new];
    //we need only 9 items. take random 9
    while ([items count] && (feedItems.count < 9)) {
        NSInteger index = arc4random_uniform([items count]);
        NSDictionary *item = items[index];
        [items removeObjectAtIndex:index];
        MGFeedItem *feedItem = [MGFeedItem modelObjectWithDictionary:item];
        [feedItems addObject:feedItem];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        block(feedItems,nil);
    });
}
@end
