//
//  MGFlickrFeedLoaderConnection.m
//  MemGame
//
//  Created by Johnykutty on 10/09/14.
//  Copyright (c) 2014 Johnykutty. All rights reserved.
//

#import "MGFlickrFeedLoaderConnection.h"

@implementation MGFlickrFeedLoaderConnection

/**
 *  Loading feeds with NSURLConnection
 *
 *  @param feedURL url to be loaded
 *  @param block completion block to be excecuted after loading feed
 */
+ (void)loadFeedWithURL:(NSURL *)feedURL completionHandler:(MGFlickrFeedLoaderCompletionBlock)block;{
    NSURLRequest *request = [NSURLRequest requestWithURL:feedURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler: ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        block(data,connectionError);
    }];
}
@end
