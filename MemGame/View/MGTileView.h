//
//  MGTileView.h
//  MemGame
//
//  Created by Johnykutty on 11/09/14.
//  Copyright (c) 2014 Johnykutty. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MGTileView;
@class MGFeedItem;

@protocol MGTileViewDelegate <NSObject>

- (void)tileViewDidLoadImage:(MGTileView *)tile;
- (void)tileViewDidTapped:(MGTileView *)tile;

@end

@interface MGTileView : UIImageView
@property(nonatomic, weak)id <MGTileViewDelegate> delegate;
@property(nonatomic, strong)MGFeedItem *feedItem;
- (void)flipToOriginalImage;
- (void)fliptoPlaceHolder;
@end
