//
//  MGTileView.m
//  MemGame
//
//  Created by Johnykutty on 11/09/14.
//  Copyright (c) 2014 Johnykutty. All rights reserved.
//

#import "MGTileView.h"
#import "SDWebImageManager.h"
#import "MGFeedItem.h"

@interface MGTileView ()
@property(nonatomic, strong)UIImage *loadedaimage;
@end

@implementation MGTileView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self configure];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self configure];
}

/**
 *  Set configuration of the tile and add tap  gesture to it
 */
- (void)configure{
    self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.contentMode = UIViewContentModeScaleAspectFit;
    self.clipsToBounds = YES;
    
    self.layer.borderWidth = 1.f;
    self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.layer.masksToBounds = YES;
    
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    [tapGestureRecognizer setNumberOfTapsRequired:1];
    [tapGestureRecognizer setNumberOfTouchesRequired:1];
    [self addGestureRecognizer:tapGestureRecognizer];
}

/**
 *  property setter for feed item
 *
 *  @param feedItem value to be set
 */
- (void)setFeedItem:(MGFeedItem *)feedItem{
    _feedItem = feedItem;
    NSLog(@"%@",feedItem.photoURL);
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:self.bounds];
    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [activityIndicator setTintColor:[UIColor redColor]];
    [activityIndicator startAnimating];

    [self addSubview:activityIndicator];
    self.userInteractionEnabled = NO;
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:feedItem.photoURL options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        [activityIndicator removeFromSuperview];
        [self setImage:[UIImage imageNamed:@"placeholder"]];
        self.loadedaimage = image;
        if ([self.delegate respondsToSelector:@selector(tileViewDidLoadImage:)]) {
            [self.delegate tileViewDidLoadImage:self];
        }
    }];
}

/**
 *  gesture recognizer action
 *
 *  @param sender UITapGestureRecognizer sending this method
 */
- (void)tapDetected:(UIGestureRecognizer *)sender{
    if ([self.delegate respondsToSelector:@selector(tileViewDidTapped:)]) {
        [self.delegate tileViewDidTapped:self];
    }
}

/**
 *  Flip to placeholder image that is, hiding original image
 */
- (void)fliptoPlaceHolder{
    self.userInteractionEnabled = YES;
    [self flip:YES];
}

/**
 *  Flip to original image that is, showing original image
 */
- (void)flipToOriginalImage{
    self.userInteractionEnabled = NO;
    [self flip:NO];
}

/**
 *  Animation is happening here
 *
 *  @param toPlaceholder boolean value that determines whether hide or show original image
 */
- (void)flip:(BOOL)toPlaceholder{
    [UIView transitionWithView:self
                      duration:0.4
                       options:toPlaceholder ? UIViewAnimationOptionTransitionFlipFromLeft : UIViewAnimationOptionTransitionFlipFromRight
                    animations:^{
                        self.image = toPlaceholder ? [UIImage imageNamed:@"placeholder"] : self.loadedaimage;
                    } completion:^(BOOL finished) {

                    }];
}
@end
