//
//  main.m
//  MemGame
//
//  Created by Johnykutty on 08/09/14.
//  Copyright (c) 2014 Johnykutty. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MGAppDelegate class]));
    }
}
